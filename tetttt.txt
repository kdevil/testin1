function fetchEmbedCodes() {
    var gistUrls = document.getElementById('gist_url_input').value.split('\n');

    embeddedCodesElement = document.getElementById('embedded-codes');
    embeddedCodesElement.innerHTML = '';

    if (gistUrls.length === 0 || (gistUrls.length === 1 && gistUrls[0] === '')) {
        return;
    }

    // Show loading indicator
    var loadingIndicator = document.getElementById('loading-indicator');
    loadingIndicator.classList.remove('d-none');

    var embedPromises = gistUrls.map(gistUrl => {
        var gistId = extractGistId(gistUrl);
        if (gistId) {
            var apiUrl;
            if (gistUrl.includes('github.com')) {
                apiUrl = "https://api.github.com/gists/" + gistId;
            } else if (gistUrl.includes('gitlab.com')) {
                apiUrl = "https://gitlab.com/api/v4/snippets/" + gistId;
            } else {
                return [];
            }

            return fetch(apiUrl)
                .then(response => {
                    if (!response.ok) {
                        throw new Error('Error fetching Gist: ' + response.status);
                    }
                    return response.json();
                })
                .then(data => {
                    var files;
                    if (gistUrl.includes('github.com')) {
                        files = data.files;
                    } else if (gistUrl.includes('gitlab.com')) {
                        files = [data];
                    }

                    var contentPromises = files.map(file => {
                        var content;
                        if (gistUrl.includes('github.com')) {
                            content = file.content;
                        } else if (gistUrl.includes('gitlab.com')) {
                            content = fetch(file.raw_url)
                                .then(response => {
                                    if (!response.ok) {
                                        throw new Error('Error fetching GitLab snippet content: ' + response.status);
                                    }
                                    return response.text();
                                })
                                .catch(error => {
                                    console.log('Error fetching GitLab snippet content:', error);
                                    return '';
                                });
                        }
                        return content;
                    });

                    return Promise.all(contentPromises);
                })
                .then(contents => {
                    for (var content of contents) {
                        var editorContainer = document.createElement('div');
                        editorContainer.className = 'form-group row form-check';
                        var textarea = document.createElement('textarea');
                        textarea.className = 'form-control gist-textarea';
                        textarea.style.width = '100%';
                        textarea.value = content;
                        editorContainer.appendChild(textarea);
                        embeddedCodesElement.appendChild(editorContainer);

                        var editor = CodeMirror.fromTextArea(textarea, {
                            mode: 'htmlmixed',
                            lineNumbers: true,
                            scrollbarStyle: 'native'
                        });
                        editors.push(editor);
                    }

                    return editors;
                })
                .catch(error => {
                    console.log(error);
                    return [];
                });
        } else {
            return [];
        }
    });

    Promise.all(embedPromises).then(allEditors => {
        // Store the CodeMirror instances
        editors = allEditors;

        // Hide loading indicator
        var loadingIndicator = document.getElementById('loading-indicator');
        loadingIndicator.classList.add('d-none');
    });
}
